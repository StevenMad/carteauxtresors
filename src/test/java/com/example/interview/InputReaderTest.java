package com.example.interview;

import com.example.interview.model.Adventurer;
import com.example.interview.model.Coord;
import com.example.interview.model.Treasure;
import com.example.interview.model.TreasureMap;
import com.example.interview.utils.TreasureMapUtils;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.stream.Stream;

public class InputReaderTest {

    private Stream<String> lines;

    @Before
    public void GivenAFile() {
        InputReader reader = new InputReader();
        InputStream inputFile = InputReaderTest.class.getClassLoader().getResourceAsStream("input.txt");
        lines = reader.readFile(inputFile);
    }

    @Test
    public void whenIParseTheFile_ThenMyTreasureMapContainsTheRightLengthAndHeight() {
        TreasureMap map = new TreasureMap();
        TreasureMapUtils.updateMap(lines, map);
        Assertions.assertThat(map.getLength()).isEqualTo(3);
        Assertions.assertThat(map.getHeight()).isEqualTo(4);
        Assertions.assertThat(map.getMountainList().size()).isEqualTo(2);
        Assertions.assertThat(map.getTreasureList().size()).isEqualTo(2);
        Treasure first = map.getTreasureList().get(0);
        Assertions.assertThat(first.getCoord()).isEqualTo(new Coord(0,3));
        Assertions.assertThat(first.getNumberOfTreasure()).isEqualTo(2);
    }

    @Test
    public void whenIParseTheFile_ThenMyAdventurerInfoWillBeFilled() {
        Adventurer adventurer = new Adventurer();
        TreasureMapUtils.updateAventurer(lines, adventurer);
        Assertions.assertThat(adventurer.getName()).isEqualTo("Lara");
        Assertions.assertThat(adventurer.getCoord()).isEqualTo(new Coord(1, 1));
        Assertions.assertThat(adventurer.getOrientation()).isEqualTo("S");
        Assertions.assertThat(adventurer.getMoves()).isEqualTo("AADADAGGA");
    }
}
