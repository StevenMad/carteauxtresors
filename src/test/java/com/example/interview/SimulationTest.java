package com.example.interview;

import com.example.interview.model.Adventurer;
import com.example.interview.model.Coord;
import com.example.interview.model.Treasure;
import com.example.interview.model.TreasureMap;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class SimulationTest {

    private Simulation simulation;

    @Before
    public void initSimulation(){
        TreasureMap map = new TreasureMap();
        map.setHeight(4);
        map.setLength(3);
        map.addMountain(1, 0);
        map.addMountain(2, 1);
        map.addTreasure(0, 3, 2);
        map.addTreasure(1, 3, 3);
        Adventurer adventurer = new Adventurer();
        adventurer.setName("Lara");
        adventurer.setCoord(new Coord(1, 1));
        adventurer.setOrientation("S");
        adventurer.setMoves("AADADAGGA");
         simulation = new Simulation(map, adventurer);
    }

    @Test
    public void testGetMoveSequence(){
        String moveSequence = "ADG";
        List<Move> moves = simulation.getMoveSequence(moveSequence);
        Assertions.assertThat(moves.size()).isEqualTo(3);
        List<Move> expectedMoves = Arrays.asList(Move.Avancer,Move.Gauche,Move.Droite);
        Assertions.assertThat(moves).containsAll(expectedMoves);
    }

    @Test
    public void testPerformMoveDroite(){
        String moveSequence = "DDD";
        List<Move> moves = simulation.getMoveSequence(moveSequence);
        simulation.performMoveSequence(moves);
        Assertions.assertThat(simulation.getAdventurer().getOrientation()).isEqualTo(Orientation.EST);
        moves = simulation.getMoveSequence("DDDD");
        simulation.performMoveSequence(moves);
        Assertions.assertThat(simulation.getAdventurer().getOrientation()).isEqualTo(Orientation.EST);
    }
    @Test
    public void testPerformMoveGauche(){
        String moveSequence = "GGG";
        List<Move> moves = simulation.getMoveSequence(moveSequence);
        simulation.performMoveSequence(moves);
        Assertions.assertThat(simulation.getAdventurer().getOrientation()).isEqualTo(Orientation.OUEST);
        moves = simulation.getMoveSequence("GGGG");
        simulation.performMoveSequence(moves);
        Assertions.assertThat(simulation.getAdventurer().getOrientation()).isEqualTo(Orientation.OUEST);
    }

    @Test
    public void testPerformMoveAvancer(){
        String moveSequence = "ADADADA";
        List<Move> moves = simulation.getMoveSequence(moveSequence);
        simulation.performMoveSequence(moves);
        Assertions.assertThat(simulation.getAdventurer().getCoord()).isEqualTo(new Coord(1,1));
    }

    @Test
    public void testPerformMoveWithMountains(){
        String moveSequence = "DDA";
        List<Move> moves = simulation.getMoveSequence(moveSequence);
        simulation.performMoveSequence(moves);
        Assertions.assertThat(simulation.getAdventurer().getCoord()).isEqualTo(new Coord(1,1));
    }

    @Test
    public void testPerformMoveOutOfBonds(){
        String moveSequence="DAAA";
        List<Move> moves = simulation.getMoveSequence(moveSequence);
        simulation.performMoveSequence(moves);
        Assertions.assertThat(simulation.getAdventurer().getCoord()).isEqualTo(new Coord(0,1));
    }

    @Test
    public void testAdventurerTakesTreasure(){
        String moveSequence = "A";
        simulation.getMap().addTreasure(1,2,1);
        List<Move> moves = simulation.getMoveSequence(moveSequence);
        simulation.performMoveSequence(moves);
        Assertions.assertThat(simulation.getAdventurer().getTreasures()).isEqualTo(1);
        Treasure actualTreasure = simulation.getMap().getTreasureList().stream().filter(treasure -> treasure.getCoord().equals(new Coord(1, 2))).findFirst().get();
        Assertions.assertThat(actualTreasure.getNumberOfTreasure()).isEqualTo(0);
    }

    @Test
    public void testSimulation() {
        simulation.runSimulation();
        Assertions.assertThat(simulation.getAdventurer().getCoord()).isEqualTo(new Coord(0, 3));
        Assertions.assertThat(simulation.getAdventurer().getOrientation()).isEqualTo(Orientation.SUD);
        Assertions.assertThat(simulation.getAdventurer().getTreasures()).isEqualTo(3);
    }

    @Test
    public void testDisplaySimulationResult(){
        simulation.runSimulation();
        InputReader reader = new InputReader();
        InputStream inputFile = InputReaderTest.class.getClassLoader().getResourceAsStream("expectedOutput.txt");
        List<String> expectedResult = reader.readFile(inputFile).collect(Collectors.toList());
        List<String> actualResult = simulation.displaySimulationState();
        Assertions.assertThat(actualResult).hasSameSizeAs(expectedResult);
        Assertions.assertThat(actualResult).containsAll(expectedResult);
    }

}
