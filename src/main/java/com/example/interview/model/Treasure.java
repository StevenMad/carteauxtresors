package com.example.interview.model;

public class Treasure {
    private Coord coord;
    private int numberOfTreasure;

    public Coord getCoord() {
        return coord;
    }

    public int getNumberOfTreasure() {
        return numberOfTreasure;
    }

    public Treasure(int x, int y, int numberOfTreasure) {
        this.coord = new Coord(x, y);
        this.numberOfTreasure = numberOfTreasure;
    }

    public void updateNumberOfTreasure() {
        numberOfTreasure--;
    }
}
