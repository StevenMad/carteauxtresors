package com.example.interview.model;

public class Mountain {

    private Coord coord;

    public Mountain(int x, int y) {
        this.coord = new Coord(x,y);
    }

    public Coord getCoord() {
        return coord;
    }
}
