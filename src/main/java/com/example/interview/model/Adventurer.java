package com.example.interview.model;

import com.example.interview.Orientation;

public class Adventurer {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Orientation getOrientation() {
        return orientation;
    }

    public void setOrientation(String orientation) {
        this.orientation=Orientation.getOrientation(orientation);
    }


    public void setOrientation(Orientation orientation) {
        this.orientation=orientation;
    }

    public Coord getCoord() {
        return coord;
    }

    public void setCoord(Coord coord) {
        this.coord = coord;
    }

    public String getMoves() {
        return moves;
    }

    public void setMoves(String moves) {
        this.moves = moves;
    }

    private Orientation orientation;
    private Coord coord;
    private String moves;

    private int treasures;
    public int getTreasures() {
        return treasures;
    }

    public void setTreasures(int treasures) {
        this.treasures = treasures;
    }
}
