package com.example.interview.model;

import java.util.ArrayList;
import java.util.List;

public class TreasureMap {
    private int length;
    private int height;

    private List<Mountain> mountainList;
    private List<Treasure> treasureList;

    public TreasureMap() {
        mountainList = new ArrayList<>();
        treasureList = new ArrayList<>();
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public List<Mountain> getMountainList() {
        return mountainList;
    }

    public List<Treasure> getTreasureList() {
        return treasureList;
    }

    public void addMountain(int x, int y) {
        mountainList.add(new Mountain(x, y));
    }

    public void addTreasure(int x, int y, int numberOfTreasure) {
        treasureList.add(new Treasure(x, y, numberOfTreasure));
    }
}
