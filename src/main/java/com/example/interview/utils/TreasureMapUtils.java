package com.example.interview.utils;

import com.example.interview.model.Adventurer;
import com.example.interview.model.Coord;
import com.example.interview.model.TreasureMap;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class TreasureMapUtils {
    public static void updateMap(Stream<String> lines, TreasureMap map) {
        lines.forEach(line -> {
            if (line.startsWith("C")) {
                List<String> collect = Arrays.stream(line.split("-")).map(String::trim).toList();
                map.setLength(Integer.parseInt(collect.get(1)));
                map.setHeight(Integer.parseInt(collect.get(2)));
            }
            if (line.startsWith("M")) {
                List<String> collect = Arrays.stream(line.split("-")).map(String::trim).toList();
                int x = Integer.parseInt(collect.get(1));
                int y = Integer.parseInt(collect.get(2));
                map.addMountain(x, y);
            }
            if (line.startsWith("T")) {
                List<String> collect = Arrays.stream(line.split("-")).map(String::trim).toList();
                int x = Integer.parseInt(collect.get(1));
                int y = Integer.parseInt(collect.get(2));
                int nbTreasures = Integer.parseInt(collect.get(3));
                map.addTreasure(x, y, nbTreasures);
            }
        });
    }

    public static void updateAventurer(Stream<String> lines, Adventurer adventurer) {
        lines.forEach(line -> {
            if (line.startsWith("A")) {
                List<String> collect = Arrays.stream(line.split("-")).map(String::trim).toList();
                adventurer.setName(collect.get(1));
                int x = Integer.parseInt(collect.get(2));
                int y = Integer.parseInt(collect.get(3));
                adventurer.setCoord(new Coord(x, y));
                adventurer.setOrientation(collect.get(4));
                adventurer.setMoves(collect.get(5));
            }
        });
    }
}
