package com.example.interview;

import com.example.interview.model.Adventurer;
import com.example.interview.model.Treasure;
import com.example.interview.model.TreasureMap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class Simulation {

    public TreasureMap getMap() {
        return map;
    }

    private TreasureMap map;
    private Adventurer adventurer;

    public Simulation(TreasureMap map, Adventurer adventurer) {
        this.map = map;
        this.adventurer = adventurer;
    }

    public Adventurer getAdventurer() {
        return adventurer;
    }

    public void runSimulation() {
        List<Move> moveSequence = getMoveSequence(adventurer.getMoves());
        performMoveSequence(moveSequence);
    }

    public List<Move> getMoveSequence(String moveSequence) {
        String[] split = moveSequence.split("(?!^)");
        return Arrays.stream(split).map(Move::getMove).toList();
    }

    public void performMoveSequence(List<Move> moves) {
        moves.forEach(move -> {
                    move.performMove(adventurer, map);
                    if (move.equals(Move.Avancer)) {
                        takeTreasure();
                    }
                }
        );
    }

    void takeTreasure() {
        Optional<Treasure> maybeATreasure = map.getTreasureList().stream().filter(item -> item.getCoord().equals(adventurer.getCoord())).findFirst();
        if (maybeATreasure.isPresent()) {
            Treasure treasure = maybeATreasure.get();
            if (treasure.getNumberOfTreasure() > 0) {
                adventurer.setTreasures(adventurer.getTreasures() + 1);
                treasure.updateNumberOfTreasure();
            }
        }
    }

    public List<String> displaySimulationState() {
        List<String> res = new ArrayList<>();
        res.add(STR."C - \{map.getLength()} - \{map.getHeight()}");
        map.getMountainList().forEach(mountain ->
                res.add(STR."M - \{mountain.getCoord().getX()} - \{mountain.getCoord().getY()}")
        );
        map.getTreasureList().forEach(treasure -> {
            if (treasure.getNumberOfTreasure() > 0) {
                res.add(STR."T - \{treasure.getCoord().getX()} - \{treasure.getCoord().getY()} - \{treasure.getNumberOfTreasure()}");
            }
        });
        res.add(STR."A - \{adventurer.getName()} - \{adventurer.getCoord().getX()} - \{adventurer.getCoord().getY()} - \{adventurer.getOrientation().getOrientationString()} - \{adventurer.getTreasures()}");
        return res;
    }
}
