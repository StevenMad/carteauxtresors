package com.example.interview;

import java.util.Arrays;

public enum Orientation {
    NORD("N"),
    SUD("S"),
    EST("E"),
    OUEST("O");

    public String getOrientationString() {
        return orientation;
    }

    private String orientation;

    public static Orientation getOrientation(String o) {
        return Arrays.stream(Orientation.values()).filter(item -> item.orientation.equals(o)).findFirst().get();
    }

    Orientation(String o) {
        this.orientation = o;
    }
}
