package com.example.interview;

import com.example.interview.model.Adventurer;
import com.example.interview.model.Coord;
import com.example.interview.model.Mountain;
import com.example.interview.model.TreasureMap;

import java.util.Arrays;

enum Move {
    Gauche("G") {
        @Override
        public void performMove(Adventurer adventurer, TreasureMap map) {
            switch (adventurer.getOrientation()) {
                case EST -> adventurer.setOrientation(Orientation.NORD);

                case NORD -> adventurer.setOrientation(Orientation.OUEST);

                case OUEST -> adventurer.setOrientation(Orientation.SUD);

                case SUD -> adventurer.setOrientation(Orientation.EST);

            }
        }
    },
    Droite("D") {
        @Override
        public void performMove(Adventurer adventurer, TreasureMap map) {
            switch (adventurer.getOrientation()) {
                case EST -> adventurer.setOrientation(Orientation.SUD);

                case NORD -> adventurer.setOrientation(Orientation.EST);

                case OUEST -> adventurer.setOrientation(Orientation.NORD);

                case SUD -> adventurer.setOrientation(Orientation.OUEST);
            }
        }
    },
    Avancer("A") {
        @Override
        public void performMove(Adventurer adventurer, TreasureMap map) {
            Coord adventurerCoord = adventurer.getCoord();
            switch (adventurer.getOrientation()) {
                case EST -> updateAdventurerMove(adventurer,map,new Coord(adventurerCoord.getX()+1, adventurerCoord.getY()));

                case NORD -> updateAdventurerMove(adventurer,map,new Coord(adventurerCoord.getX(),adventurerCoord.getY()-1));

                case OUEST -> updateAdventurerMove(adventurer,map,new Coord(adventurerCoord.getX()-1, adventurerCoord.getY()));

                case SUD -> updateAdventurerMove(adventurer,map,new Coord(adventurerCoord.getX(), adventurerCoord.getY()+1));
            }
        }
    };

    private static void updateAdventurerMove(Adventurer adventurer,TreasureMap map, Coord coord) {
        boolean isMountainPresent = map.getMountainList().stream().anyMatch(item -> item.getCoord().equals(coord));
        int x = coord.getX();
        if(x<0){
            coord.setX(0);
        }
        if(x>=map.getLength()){
            coord.setX(map.getLength()-1);
        }
        int y = coord.getY();
        if(y<0){
            coord.setY(0);
        }
        if(y>=map.getHeight()){
            coord.setY(map.getHeight()-1);
        }
        if(!isMountainPresent){
            adventurer.setCoord(coord);
        }
    }

    private String move;

    static Move getMove(String s) {
        return Arrays.stream(Move.values()).filter(item -> item.move.equals(s)).findFirst().get();
    }

    Move(String move) {
        this.move = move;
    }

    public abstract void performMove(Adventurer adventurer, TreasureMap map);
}
